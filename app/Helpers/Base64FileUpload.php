<?php
/**
 * Created by VScode.
 * User: Hasinur Rahman
 * Date: 08/09/2022
 * Time: 12:56 PM
 */

namespace App\Helpers;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Validator;

class Base64FileUpload {

    /*Check Dir Permission*/
    public static function is_dir_set_permission($directory){
        if(is_dir(storage_path($directory))) {
            Base64FileUpload::check_permission($directory);
            return true;
        }
        else
        {
            Base64FileUpload::make_directory($directory);
            return true;
        }

    }

   /*Make Diretory*/
    protected static function make_directory($directory) {
        File::makeDirectory(storage_path($directory), 0777, true, true);
        return true;
    }

    /*Check Permission*/
    protected static function check_permission($directory){
        if(is_writable(storage_path($directory)))
        {
            return true;
        }
        else
        {
            Base64FileUpload::set_permission($directory);
            return true;
        }
    }

    /*Set Permission*/
    protected static function set_permission($directory){
        if(!is_dir(storage_path($directory))){
            File::makeDirectory(storage_path($directory), 0777, true, true);
            return true;
        }
        return false;
    }

    /**
     * base64 Image Validation
     */
    public static function base64_image_validation ($image, $request_file_name, $fileSize = null) {
        $validator = Validator::make([], []); // Empty data and rules fields
        try {
            $allow = ['jpeg', 'jpg', 'png', 'gif'];
            $extension = explode('/', mime_content_type($image))[1];

            // checking file size 
            if ($fileSize) {
                $size = strlen(base64_decode($image));
                $size_kb = $size / 1024;
                if ($size_kb > (int)$fileSize) {
                    $fileMb = (int)$fileSize > 1024 ? ((int)$fileSize / 1024).' MB' : $fileSize. 'KB';

                    return ([
                        'success' => false,
                        'message' => 'Not validate Image',
                        'errors' => $validator->errors()->add($request_file_name, 'Maximum file size must be '.$fileMb)
                    ]);
                }
            }

            $isExist = collect($allow)->contains($extension);
            if (!$isExist) {
                $name = str_replace('_', ' ', $request_file_name);
                $message = 'The '.$name.' must be a file of type: png, gif, jpeg, jpg.';

                return ([
                    'success' => false,
                    'message' => 'Not validate Image',
                    'errors' => $validator->errors()->add($request_file_name, $message)
                ]);
            } else {
                return (['success' => true, 'extension' => $extension]);
            }
        } catch (\Exception $e) {
            return ([
                'success' => false,
                'message' => 'Not validate Image',
                'errors' => $validator->errors()->add($request_file_name, 'Not validate Image')
            ]);
        }
    }

    /**
     * Upload image to specified folder
     *
     * @param object $imageFile The base64 file data
     * @param string $fieldName The name of input file field
     * @param string $destinationPath The name of upload path
     * @param mixed|string|null $oldFile The value of db table column default null
     * @param int $width The width of expected image
     * @param mixed|int|bool $height The height of expected image default null
     * @return array
     */
    public static function uploadImage($image_file, $fieldName, $destinationPath, $oldFile = null, $width = null, $height = null, $size = null)
    {
        $destinationPath = 'app/public/'.$destinationPath;
        // Image Validation
        $validation = Self::base64_image_validation($image_file, $fieldName, $size);
        if (!$validation['success']) {
            return $validation;
        }
        
        $ext = $validation['extension'];
        $image = str_replace('data:image/'.$ext.';base64,', '', $image_file);
        $file  = str_replace(' ', '+', $image);

        $imageResize = Image::make(base64_decode($file));
        $imageName = rand(100000, 999999) . time() . '.' . $ext;
        
        $defaultWidth = !empty($width) ? $width : 500;
        $defaultHeight = !empty($height) ? $height : 500;
        $orgWidth  = $imageResize->width();
        $orgHeight = $imageResize->height();

        if ($orgWidth >= $orgHeight) {
            $imageResize->resize($defaultWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            $imageResize->resize(null, $defaultHeight, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        Self::is_dir_set_permission($destinationPath);
        $imageResize->save(storage_path($destinationPath.$imageName));

        if ($oldFile != null) {
            Self::deleteFile($destinationPath, $oldFile);
        }

        return [
            'success' => true,
            'message' => 'File uploaded successfully',
            'data' => $imageName
        ];
    }


    /**
     * base64 File Validation
     */
    public static function base64_file_validation ($file, $request_file_name, $fileSize = null) {
        $validator = Validator::make([], []); // Empty data and rules fields
        try {
            $allow = ['pdf', 'vnd.ms-excel', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'vnd.openxmlformats-officedocument.wordprocessingml.document'];
            $extension = explode('/', mime_content_type($file))[1];
            // checking file size 
            if ($fileSize) {
                $size = strlen(base64_decode($file));
                $size_kb = $size / 1024;
                if ($size_kb > (int)$fileSize) {
                    $fileMb = (int)$fileSize > 1024 ? ((int)$fileSize / 1024).' MB' : $fileSize. 'KB';

                    return ([
                        'success' => false,
                        'message' => 'Not validate Image',
                        'errors' => $validator->errors()->add($request_file_name, 'Maximum file size must be '.$fileMb)
                    ]);
                }
            }

            $isExist = collect($allow)->contains($extension);
            if (!$isExist) {
                $name = str_replace('_', ' ', $request_file_name);
                $message = 'The '.$name.' must be a file of type: pdf, xls, xlsx, doc, docx, csv.';

                return ([
                    'success' => false,
                    'message' => 'Not validate file',
                    'errors' => $validator->errors()->add($request_file_name, $message)
                ]);
            } else {
                return (['success' => true, 'extension' => $extension]);
            }
        } catch (\Exception $e) {
            return ([
                'success' => false,
                'message' => 'Not validate File',
                'errors' => $validator->errors()->add($request_file_name, 'Not validate File')
            ]);
        }
    }


    /**
     * Upload image to specified folder
     *
     * @param object $imageFile The base64 file data
     * @param string $fieldName The name of input file field
     * @param string $destinationPath The name of upload path
     * @param mixed|string|null $oldFile The value of db table column default null
     * @return array
     */
    public static function uploadFile($main_file, $fieldName, $destinationPath, $oldFile = null, $size = null)
    {
        $destinationPath = 'app/public/'.$destinationPath;
        // File Validation
        $validation = Self::base64_file_validation($main_file, $fieldName, $size);
        if (!$validation['success']) {
            return $validation;
        }

        $extension = $validation['extension'];

        if ($extension == 'plain') {
            $new_ext = 'csv';
        } else if ($extension == 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            $new_ext = 'xlsx';
        } else if ($extension == 'vnd.ms-excel') {
            $new_ext = 'xls';
        }
        // else if ($extension == 'html') {
        //     $new_ext = 'xls';
        // }
        else if ($extension == 'vnd.openxmlformats-officedocument.wordprocessingml.document') {
            $new_ext = 'docx';
        } else if ($extension == 'csv') {
            $new_ext = 'csv';
        } else {
            $new_ext = $extension;
        }

        Self::is_dir_set_permission($destinationPath);

        $appData = mime_content_type($main_file);
        $file = str_replace('data:'.$appData.';base64,', '', $main_file);
        $new_file = str_replace(' ', '+', $file);
        $fileName = uniqid() . '.'.$new_ext;

        file_put_contents(storage_path($destinationPath.$fileName), base64_decode($new_file));

        if ($oldFile != null) {
            Self::deleteFile($destinationPath, $oldFile);
        }

        return [
            'success' => true,
            'message' => 'File uploaded successfully',
            'data' => $fileName
        ];
    }

    /**
     * Delete the file path
     *
     * @param string $oldFile The file path to be deleted
     * @return void
     */
    public static function deleteFile($path, $oldFile)
    {
        $filePathToDelete = storage_path($path. $oldFile);

        if (file_exists($filePathToDelete)) {
            File::delete($filePathToDelete);
        }
    }
    private static function checkDirectory($dir)
    {
        File::makeDirectory(storage_path($dir), 0777, true, true);
    }

    public static function base64FileValidation2 ($request, $fieldName, $fileSize = null)
    {
        $validator = Validator::make([], []); // Empty data and rules fields

        try {
            $file = $request[$fieldName];
            $file_parts = explode(";base64,", $request[$fieldName]);
            $file_type_aux = explode("/", $file_parts[0]);
            $allow = ['pdf', 'vnd.ms-excel', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'msword', 'vnd.openxmlformats-officedocument.wordprocessingml.document'];
//            $extension = explode('/', mime_content_type($file))[1];
            $extension = $file_type_aux[1];
            // checking file size
            if ($fileSize) {
                $size = strlen(base64_decode($file));
                $size_kb = $size / 1024;

                if ($size_kb > (int)$fileSize) {
                    $fileMb = (int)$fileSize > 1024 ? ((int)$fileSize / 1024).' MB' : $fileSize. 'KB';

                    return ([
                        'success' => false,
                        'message' => 'Not validate Image',
                        'errors' => $validator->errors()->add($fieldName, 'Maximum file size must be '.$fileMb)
                    ]);
                }
            }

            $isExist = collect($allow)->contains($extension);

            if (!$isExist) {
                $name = str_replace('_', ' ', $fieldName);
                $message = 'The '.$name.' must be a file of type: pdf, xls, xlsx, doc, docx, csv.';

                return ([
                    'success' => false,
                    'message' => 'Not validate file',
                    'errors' => $validator->errors()->add($fieldName, $message)
                ]);
            } else {
                return (['success' => true, 'extension' => $extension]);
            }
        } catch (\Exception $e) {
            return ([
                'success' => false,
                'message' => 'Not validate File',
                'errors' => $validator->errors()->add($fieldName, 'Not validate File')
            ]);
        }
    }

    /**
     * Upload file
     *
     * @param object $request
     * @param string $fieldName The name of input file field
     * @param int|null $fileSize The max file size of file default null
     * @return array
     */
    public static function uploadFile2($request, $fieldName, $destinationPath, $oldFile = null, $fileSize = null)
    {
        if (Str::contains($request[$fieldName], 'data:image')) {
            return self::uploadImage($request, $fieldName, $destinationPath, $oldFile, null, null, $fileSize);
        }

        $validationResult = self::base64FileValidation2($request, $fieldName, $fileSize);

        if (!$validationResult['success']) {
            return $validationResult;
        }

        if (!$fieldName) {
            return ['success' => false, 'message' => 'File not uploaded'];
        }

        if (!Str::startsWith($destinationPath, '/')) {
            $destinationPath = '/' . $destinationPath;
        }

        $finalDestinationPath = 'app/public' . $destinationPath;
        self::checkDirectory($finalDestinationPath);

        if ($oldFile != null) {
            self::deleteFile($oldFile);
        }

        $file_parts = explode(";base64,", $request[$fieldName]);
        $file_type_aux = explode("/", $file_parts[0]);
        $ext = $file_type_aux[1];
        $appData = [];

        if ($ext == 'pdf') {
            $appData = 'application/pdf';
        } else if ($ext == 'vnd.ms-excel') {
            $ext = 'xls';
            $appData = 'application/vnd.ms-excel';
        } else if ($ext == 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            $ext = 'xlsx';
            $appData = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        } else if ($ext == 'vnd.openxmlformats-officedocument.wordprocessingml.document') {
            $ext = 'docx';
            $appData = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        } else if ($ext == 'msword') {
            $ext = 'doc';
            $appData = 'application/msword';
        } else if ($ext == 'csv') {
            $appData = 'text/csv';
        }

        if (!$appData) {
            return [
                'success'   => false,
                'message'   => 'File extension is not supported'
            ];
        }

        $mainFile = $request[$fieldName];

        $image = str_replace('data:'.$appData.';base64,', '', $mainFile);
        $image = str_replace(' ', '+', $image);
        $filePath = 'storage'.$destinationPath.'/'. uniqid() . '.'.$ext;
        file_put_contents($filePath, base64_decode($image));

        return [
            'success'   => true,
            'message'   => 'File uploaded',
            'data'      => $filePath
        ];
    }
}
