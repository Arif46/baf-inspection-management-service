<?php

namespace App\Http\Controllers\Specification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Specification\{
    Item,
    ItemSpecification, 
    SupplierItem,
    SupplierItemSpecification 
};
use App\Http\Validations\Specification\SpecificationVerifyValidations;
use Illuminate\Support\Facades\DB;
class SpecificationVerfyController extends Controller
{
    /**
     * @queryParam Specification verify list
     */
    public function index(Request $request)
    {
        $query = SupplierItem::with([
            'supplier_specification_details.childs.specification_item',
            'supplier_specification_details.specification_item',
        ])->latest();

        if ($request->supplier_name) {
            $query = $query->where('supplier_name', 'like', "{$request->supplier_name}%");
        }

        $list = $query->paginate($request->limit);

        if (!$list) {

            return response()->json([
                'success' => false,
                'message' => 'Data Not Found'
            ]);

        } else {
            return response()->json([
                'success' => true,
                'data' => $list,
                'message' => 'Supplier Info List'
            ],201);
        }
    }

    /**
     * @queryParam Specification info list
     */
    public function verifyInfoList(Request $request)
    {
        $query = Item::with('specificationDetails.childs');

        if ($request->demand_no) {
            $query = $query->where('demand_no', $request->demand_no);
        }  
        
        $list = $query->first();

        if (!$list) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Data found.',
            'data' => $list
        ]);      
    }

    /**
     * @queryParam Specification verify 
     * @queryParam Store
     */
    public function store(Request $request)
    {
        $validationResult = SpecificationVerifyValidations::validate($request);  

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        DB::beginTransaction();

        try {
            $requestAll = $request->all();
            $model = SupplierItem::create([
                'demand_no' => $request->demand_no,
                'item_id' => $request->id,
                'supplier_name' => $request->supplier_name,
                'total_fully_comply' => $request->total_fully_comply,
                'total_partially_comply' => $request->total_partially_comply,
                'total_reject' => $request->total_reject
            ]);

            // $specificationData = [];
            foreach ($request->specification_details as $specificationItem) {
                $specificationData = [];
                if ($specificationItem['has_child']) {
                    unset($specificationData['childs']);
                }
                $specificationData['specification_id'] = $specificationItem['id'];
                $specificationData['received_specification'] = $specificationItem['received_specification'];
                $specificationData['verify_status'] = $specificationItem['verify_status'];
                $specificationData['reason'] = $specificationItem['reason'] ?? '';
                $specificationModel = $model->supplierSpecifications()->create($specificationData);
            
                if ($specificationItem['has_child']) {
                    $childsArr = $specificationItem['childs'];
                    $arr = [];
                    foreach ($childsArr as $key => $item) {
                        $arr[$key]['supplier_item_id'] = $model->id;
                        $arr[$key]['specification_id'] = $item['id'];
                        $arr[$key]['verify_status'] = $specificationItem['verify_status'];
                        $arr[$key]['received_specification'] = $specificationItem['received_specification'];
                        $arr[$key]['reason'] = $specificationItem['reason'] ?? '';
                    }
                    $specificationModel->childs()->createMany($arr);
                }
            }

            DB::commit();

        } catch (\Exception $ex) {
            DB::rollBack();
            return [
                'success' => false,
                'message' => 'Failed to saved data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : []
            ];
        }   

        return response()->json([
            'success' => true,
            'message' => 'Data Save Successfully',
            'data' => $model
        ],200);
    }
}
